# Collatz

Supplemental material for the manuscript showing that $`\sigma_\infty(n) \geq 6.9521 \ln n`$ for infinitely many $`n`$.

Files:
- cert-14.txt: Certificate up to l=14 (242,491 entries)
- l15.txt: Open vectors for l=15 (2,563,281 entries)
- make_cert.py: Python code for generating a certificate for small length. We set LEN=14 to make cert-14.txt (and l15.txt by a slightly modfied code).
- make_ex.py: Python code for generating look-up tables used in make_cert.c for pruning. 
   (Please save the output as exX.txt for X=6,10,14,16 that are needed to execute make_cert.c).
- make_cert.c: C code for finding a certificate. This is the main body of our project.  
   Required: l15.txt (list of open vectors) and ex??.txt (look-up tables for pruning).  
      
   Usage: **./make_cert start width [v]**   
    For example, run **make_cert 100000 1000** to find a certificate for the #100000-th to #100999-th open vectors in l15.txt (counting from 0). The output seems like:  
    ```
        F 1703 L29 [0 0 0 6 23 51 94 191 264 400 340 206 91 31 6 0 0 0 0 0 0 0 0 0 0 ] 100001  
        F 7173 L30 [0 0 0 4 10 40 140 350 716 1284 1579 1471 1079 397 79 24 0 0 0 0 0 0 0 0 0 ] 100002  
    ```

    Each line consists of the letter "F", total # of critical paths, max level, the list representing # of critical paths of weight l=15,16,..., and a sequential # in the list of open vectors (l15.txt, counting from 1). It just verifies the existence of a certificate and counts critical paths.  
    Add "v" optition to actually output the description of the certificate.  
      
    Warning: If you run the code for generating whole certificate at once, then it will take about one year and produce ~ 2 Tera bytes of data.
- decode.py: Python code for converting the output of make_cert.c to a certificate file. Give the file name of the output of make_cert.c (in the verbose mode) as the argument to decode.py.

   A certificate file seems like:  
   ```
      N 0 10 1 01 1 1  
      N 1 20 1 1 2 1  
      N 2 11 1 01 4 5  
      N 3 22 1 1 8 5  
      N 4 2100 3 001011 5 11  
      N 5 2102 3 001011 59 139  
    ```

   Each line consists of a letter "N", a sequencial number, $`a \pmod {3^{l+1}}`$ (in reverse ternaly), $`l`$, critical path, $`a`$ (in decimal) and an integer at the terminal of the critical path when starting from $`a`$.
