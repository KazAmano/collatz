# Generate a list of (alpha, z(alpha)) (a is in mod 3^L),
# where z(alpha) is the max of "#1" - "#0" in the first i-level of
# a tree starting from alpha. The value of i ranges from 0 to L-1.

from collections import deque
import copy

L = 6

C = deque()

for i in range(1,3**L):
  if i%3 != 0:
    C.append([i,L-1])

while len(C)!=0:
  if len(C)>100000000:
    break

  now=C.popleft()
  st=now[0]
  s=now[0]
  l=now[1]
#  print("Test", s,l)
  Z = deque()
  sq=[]
  Z.append([s,0,sq])
  
  k=0
  for k in range(1,L):
#    print(k, Z)
    Zn = deque()
    while len(Z)>0: 
      S = Z.popleft()
#      print("Now", S)
      s = S[0] 
      if s%9==1 or s%9==4 or s%9==5 or s%9==7:
        t=s*2
        sq = copy.copy(S[2])
        sq.append(t%2)
        Zn.append([t, S[1]+(t%2), sq])
      elif s%9==2 or s%9==8:
        t=(2*s-1)//3
        sq = copy.copy(S[2])
        sq.append(t%2)
        Zn.append([t, S[1]+(t%2), sq])
        t=2*s
        sq = copy.copy(S[2])
        sq.append(t%2)
        Zn.append([t, S[1]+(t%2), sq])
    Z=Zn.copy()
#  print(Z)
  
  allmx=0
  
  while len(Z)>0:
    now=Z.popleft()
    l=now[2]
    ex=0
    mx=0
#    print(l, len(l))
    for i in range(len(l)):
      if l[i]==0:
        ex=ex-1
      else:
        ex=ex+1
      if mx<ex:
        mx=ex
    if allmx<mx:
      allmx=mx
  print(st, allmx)
    
