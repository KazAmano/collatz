/* Find a Certificate for \gamma(n) > \gamma_0 infinitely often */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define LI long long int
#define Exp 60
#define Base ((LI)(1)<<Exp)

#define E3 (59049LL*9*9*9)
#define ELen 16

#define Etwo3 (59049LL*9*9)
#define EtwoLen 14

#define Ethree3 (59049LL)
#define EthreeLen 10

#define Efour3 (729LL)
#define EfourLen 6

#define Lstart 15

#define CMax 50000000
#define ZMax 50000000

#define LEN 40
#define Lstop 40

#define L15 2563281
#define Lwidth 1

LI Lin[L15]={0};

FILE *fp;

LI C[CMax];
int Cl[CMax];
int Cpos=0, Cs=0;
int CertHist[LEN];
long long int CertAll[LEN]={0}, sumCert=0;
int verb=0;

LI ZH[ZMax]={0}; /* high */
LI ZL[ZMax]={0}; /* low */
int Zl[ZMax];
int Zpos=0, Zs=0, Zt=0;
int ZposMax=0;

int Hist[LEN]={0};
LI HistAll[LEN]={0};

LI P3[LEN]={0};

int E[E3]={0}, Etwo[Etwo3]={0}, Ethree[Ethree3]={0}, Efour[Efour3]={0};
int Linpos=0;

int B9=(Base)%9;
int B3=(Base)%3;

int mod(LI h, LI l, int p);
int mod9(LI h, LI l);
int mod3(LI h, LI l);
void init();
void step(LI h, LI l, LI *hs, LI *ls);
void dbl(LI h, LI l, LI *hs, LI*ls);
int cal();

int main(int argc, char *argv[]){
  int i, j;
  int Lposstart, Lposend;
  time_t ti_s, ti_t;


  if ((argc<3) || (argc>4)) {
    printf("foo start width [v]\n"); exit(0);
  }
  if (argc==4) verb=1;

  init();
  Lposstart=atoi(argv[1]);	
  Lposend=atoi(argv[2])+Lposstart;	
  if (Lposend>L15) Lposend=L15;

  ti_s=time(NULL);
  printf("C From %d to %d : start %s", Lposstart+1, Lposend, ctime(&ti_s));
  Linpos=Lposstart;
  for (i=0; ;i++){
    Cpos=0; Cs=0;
    for (j=0; j<Lwidth; j++){
      C[Cpos]=Lin[Linpos]; Cl[Cpos]=15; Cpos++; Linpos++;
      if (Linpos>=Lposend) break;
    }
    for (i=0; i<LEN; i++) Hist[i]=0;
    cal();
    for (i=Lstart; i<LEN; i++) CertAll[i]+=CertHist[i];
    if (Linpos>=Lposend) break;
  }
  ti_t=time(NULL);
  printf("C Elapsed time %d (sec) : end %s", (int)(ti_t-ti_s), ctime(&ti_t));
  printf("C [");
  for (i=Lstart; i<LEN; i++) {
    printf("%lld ", CertAll[i]); sumCert+=CertAll[i];
  }
  printf("] %lld\n", sumCert);
/*
  for (i=0; i<9; i++){
    if (i%3!=0) { C[Cpos]=i; Cl[Cpos]=1; Cpos++; }
  }
*/
}

int cal(){
  LI s0, s, shh, sll, ha, la, sh, sl, sh0, sl0;
  int i, z, k, l0, l2, j, prev, ex, numT=0, s_mod9, s_mod3, s_mod27, h0, h1, s_modE;
  prev=0;

  for (i=0; i<LEN; i++) CertHist[i]=0;
  while (Cs<Cpos) {
    s0=C[Cs];
    l0=Cl[Cs]; l2=2*l0;
    Cs++;

    if (prev!=l0) {
      Hist[l0]=(Cpos-Cs)+1;
      prev=l0;
      h0=h1=0;
    }
    if (l0==Lstop) break;

    ZH[0]=s0/Base; ZL[0]=s0%Base; Zl[0]=0; Zpos=1; Zs=0;
//    printf("Test(%d) %lld, %d\n", Cs-1, s0, l0);

    for (k=0; k<=l2-2; k++){
//      printf("k=%d start\n", k);
      ex=0;
      Zt=Zpos;
      if (k>=l2-2) {
        for (z=Zs; z<Zpos; z++){
          if (Zl[z]*2>=k) { ex=1; shh=ZH[z]; sll=ZL[z]; break; }
          if ((Zl[z]+2)*2>=l2) {
            s_mod27=mod(ZH[z], ZL[z],27);
            if ((s_mod27==17) || (s_mod27==26)) { 
              ex=1; 
              step(ZH[z], ZL[z], &sh0, &sl0);
              step(sh0, sl0, &shh, &sll);
              break; 
            }
          }
        }
      } else {
//       printf("from %d to %d\n", Zs, Zpos-1);
        for (z=Zs; z<Zt; z++){
          if (Zl[z]<k-l0) continue;
          sh=ZH[z]; sl=ZL[z];
//          printf("Check %lld:%lld with %d at %d\n", sh, sl, Zl[z], z); 
          if (k!=0) {
            if (Zl[z]*2 >= k) { ex=1; shh=sh; sll=sl; break; }
          }

          if (k==l2-ELen) {
            s_modE = mod(sh, sl, E3);
            if (k-2*Zl[z]-1 > E[s_modE]) continue;
          } else if (k==l2-EtwoLen) {
            s_modE = mod(sh, sl, Etwo3);
            if (k-2*Zl[z]-1 > Etwo[s_modE]) continue;
          } else if (k==l2-EthreeLen) {
            s_modE = mod(sh, sl, Ethree3);
            if (k-2*Zl[z]-1 > Ethree[s_modE]) continue;
          } else if (k==l2-EfourLen) {
            s_modE = mod(sh, sl, Efour3);
            if (k-2*Zl[z]-1 > Efour[s_modE]) continue;
          } 

          s_mod9 = mod9(sh, sl);
          if (k<l2-2) {
            if ((s_mod9==2) || (s_mod9==8)) {
              step(sh, sl, &shh, &sll);
              ZH[Zpos]=shh; ZL[Zpos]=sll; Zl[Zpos]=Zl[z]+1; 
              Zpos++;
              if (Zpos>=ZMax) { printf("Error! Zpos %d Exceeds\n", ZposMax); exit(0); }
            }
            s_mod27= mod(sh, sl, 27);
            if ((k>=l2-4) && (s_mod27!=13) && (s_mod27!=22)) continue;  /* this is safe only for l >= 5 or so */
            s_mod3 = mod3(sh, sl);
            if (s_mod3!=0) {
              dbl(sh, sl, &shh, &sll);
              ZH[Zpos]=shh; ZL[Zpos]=sll; Zl[Zpos]=Zl[z]; 
//             printf("Add(%d) %lld %d\n", Zpos, Z[Zpos], Zl[Zpos]);
              Zpos++;
              if (Zpos>=ZMax) { printf("Error! Zpos %d Exceeds\n", ZposMax); exit(0); }
            }
          } else if (k>=l2-2) { /* actually never come here */
            if ((s_mod9==2) || (s_mod9==8)) {
              step(sh, sl, &shh, &sll);
              ZH[Zpos]=shh; ZL[Zpos]=sll; Zl[Zpos]=Zl[z]+1; 
//              printf("Add(%d) %lld %d\n", Zpos, Z[Zpos], Zl[Zpos]);
              Zpos++;
              if (Zpos>=ZMax) { printf("Error! Zpos %d Exceeds\n", ZposMax); exit(0); }
            }
          }
        }
        Zs=Zt;
      }
      if (ex) {
        numT++;
        h1++;
        CertHist[l0]++;
        /* shh, sll is the solution */
        if (verb==1) {
          printf("%d %d %lld %lld %lld %d\n", numT, l0, s0, shh, sll, l2);
        }
/*
        if (numT%100==0) {
          printf("N %d L%d %lld %lld [", numT, l0, shh, sll);
          for (i=0; i<LEN; i++) printf("%d ", Hist[i]);
          printf("]\n");
        }
*/
        break;
      }
    }
    h0++;
    if (Zpos>ZposMax) {
      ZposMax=Zpos;
      if (Zpos>=ZMax) { printf("Error! Zpos %d Exceeds\n", ZposMax); exit(0); }
    }
    if (ex==0) {
//      if (l0>=LEN) exit(0);
      if (l0<Lstop) {
        for (j=0; j<3; j++){
          C[Cpos]=s0+P3[l0+1]*j; Cl[Cpos]=l0+1; 
//        printf("Append(%d) %lld %d\n", Cpos, C[Cpos], Cl[Cpos]);
          Cpos++;
        }
      }
      if (Cpos>=CMax) {
        printf("C Exceeds\n"); 
      }
    }
  }
//  printf("ZposMax=%d\n", ZposMax); 
  printf("F %d L%d [", numT, l0);
  for (i=Lstart; i<LEN; i++) {
    HistAll[i]+=Hist[i];
    //printf("%lld ", HistAll[i]);
    printf("%d ", CertHist[i]);
  }
  printf("] %d\n", Linpos);
}

/* return( h*Base+l % p) */
int mod(LI h, LI l, int p){
  int a;
  a=Base%p;
  return(((h*a)%p+l)%p);
}

int mod9(LI h, LI l){ return((h*B9+l)%9); }
int mod3(LI h, LI l){ return((h*B3+l)%3); }

void init(){
  int i, x, y;
  LI tmp=1;

  for (i=0; i<LEN; i++){
    P3[i]=tmp;
    tmp=tmp*3LL;
  }

  fp = fopen("ex16.txt", "r");
  if (fp==NULL){
    printf("Error\n");
    exit(-1);
  }
  for (;;){
    if (fscanf(fp, "%d %d", &x, &y)==EOF) break;  
    E[x]=y;
  }

  fp = fopen("ex14.txt", "r");
  if (fp==NULL){
    printf("Error\n");
    exit(-1);
  }
  for (;;){
    if (fscanf(fp, "%d %d", &x, &y)==EOF) break;  
    Etwo[x]=y;
  }

  fp = fopen("ex10.txt", "r");
  if (fp==NULL){
    printf("Error\n");
    exit(-1);
  }
  for (;;){
    if (fscanf(fp, "%d %d", &x, &y)==EOF) break;  
    Ethree[x]=y;
  }


  fp = fopen("ex6.txt", "r");
  if (fp==NULL){
    printf("Error\n");
    exit(-1);
  }
  for (;;){
    if (fscanf(fp, "%d %d", &x, &y)==EOF) break;  
    Efour[x]=y;
  }
  fp = fopen("l15.txt", "r");
  if (fp==NULL){
    printf("Error\n");
    exit(-1);
  }

  for (i=0;;i++){
    if (fscanf(fp, "%lld", &tmp)==EOF) break;
    Lin[i]=tmp;
  }
}

/* calculate (2s-1)/3 */
void step(LI h, LI l, LI *hs, LI *ls){
  LI tmp, tmp2;
  if (l==0) { l=Base; h--; }
  *hs = (2LL*h)/3;
  tmp = (2LL*h)%3;
  tmp= tmp*Base+(2LL*l-1);

  tmp2 = tmp/3;
  *ls = tmp2&(Base-1LL);
  *hs = *hs+((tmp2&Base)?1:0);
}

/* calculate (2s) */
void dbl(LI h, LI l, LI *hs, LI*ls){
  LI tmp;
  *ls=(l<<1)&(Base-1LL);
  *hs=(h<<1)+(((l<<1)&Base)?1:0);
}
