# Generate a certificate for alpha of length up to LEN

from collections import deque
import copy

LEN = 14

C = deque()
alpha = 1/2
eps = 0.00001
print("alpha=", alpha)

Hist=[0]*(LEN+1)

Zmax=0
numT=0
for i in range(1,10):
  if i%3 != 0:
    C.append([i,1])


while len(C)!=0:
  if len(C)>10000000:
    break

  now=C.popleft()
  s=now[0]
  l=now[1]
  if (l>LEN):
    print(Hist)
    print(len(C)+1, "open vectors.");
    exit()
#  print("Test", s,l)
  Z = deque()
  sq=[]
  Z.append([s,0,sq])
  
  k=0
  while True:
    if len(Z)>Zmax:
      Zmax=len(Z)
#    print(k, Z)
    Zn = deque()
    ex=0
    while len(Z)>0: 
      S = Z.popleft()
#      print("Now", S)
      if k!=0:
        if S[1]/k >= alpha-eps:
          ex=1
          break
      s = S[0] 
      if s%9==1 or s%9==4 or s%9==5 or s%9==7:
        t=s*2
        sq = copy.copy(S[2])
        sq.append(t%2)
        Zn.append([t, S[1]+(t%2), sq])
      elif s%9==2 or s%9==8:
        t=(2*s-1)//3
        sq = copy.copy(S[2])
        sq.append(t%2)
        Zn.append([t, S[1]+(t%2), sq])
        t=2*s
        sq = copy.copy(S[2])
        sq.append(t%2)
        Zn.append([t, S[1]+(t%2), sq])
    if ex==1:
      print("N", numT, end=' ')
      m=now[0]
      for j in range(l+1):
        print(m%3, end='')
        m=m//3
      print('', S[1], end=' ')
      st=S[2]
      for j in range(len(st)):
        print(st[j], end='')
      print('', now[0], S[0])
      Hist[S[1]]+=1
      numT=numT+1
      break
    if k*alpha>l:
#      print("NO at ", k)
      for j in range(3):
        C.append([now[0]+3**(l+1)*j, l+1])
#        print("append", now[0]+3**(l+1)*j, l+1)
      break
    k=k+1
    Z=Zn.copy()

print("Finish.")
print(Hist)

